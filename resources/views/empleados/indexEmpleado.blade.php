@extends('layouts.app')
@section('titulo','Ver Contratados')

@section('content')
{{-- @if (Auth::user()->rol == 'Administrador') --}}

@include("errors.alert-danger")
@include("errors.alert-success")
@include("errors.alert-warning")
@include("errors.custom-alert")

<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Empleados</h4>
    </div>
</div>
<div class="row offset-lg-1">
    <div class="col-md-12">
        <div class="col-md-3">
            <a href="{{url('empleado/create')}}" class="btn btn-primary">
                Nuevo Registro
            </a>
        </div>       
    </div>
</div>
</br>

{{-- LAS CARDS AQUI --}}
<div class="row offset-lg-1">
    @foreach($allEmployee as $valor)
    <div class="card" style="width: 12rem; height: 22rem; margin: 10px;">
        <img class="card-img-top" style="width: 12rem; height:12rem;" src="{{asset('storage/'.$valor->avatar)}}"
            alt="Card image cap">
        <div class="card-body">
            <p class="card-text">{{$valor->first_name." ".$valor->middle_name." ".$valor->last_name}}</p>            
            <a href="{{url('/empleado/'.encrypt($valor->id).'/edit')}}" class="btn btn-primary">Editar</a>

            <a href="#" class="btn btn-danger>
                <form action=" {{ route('empleado.inactivar',$valor->id) }}" method="DELETE" id="form_deleted_col">
                <input type="hidden" name="_method" value="DELETE">
                {{ csrf_field() }}
                <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                <button class="btn btn-danger botonCus" type="submit"
                    onclick="return AlertEliminar(event,{{ $valor->id }})">
                    <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">

                    <input class="btn btn-danger" style="width:50" type="image" src="img/eliminar.png" />
                </button>
                </form>
            </a>
        </div>
    </div>
    @endforeach
</div>

@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- <script src="{{url('js/jquery.dataTables.js')}}"></script> -->
<script type="text/javascript">
function AlertEliminar(event, id) {
    event.preventDefault();
    // var route = BASE_URL + "cuarto/borrar/" + id;
    var route = BASE_URL + "empleado/inactivar/" + id;
    // alert (route);
    swal({
        title: "¿Está seguro de eliminar este registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, Eliminalo!",
        closeOnConfirm: false
    }, function(inputValue) {
        if (inputValue === false) return false;

        // var form = $("#form_deleted_col").attr("action", route);
        // $("#form_deleted_col").submit();
        window.location = route;
        swal("Correcto!", "Se elimino correctamente", "success");
    });
}
</script>