@extends('layouts.app')
@section('titulo','Nuevo Registro')

@section('content')
<style>
.error {
  color: #af5157;
}
</style>
<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var phones = [{ "mask": "(###) ###-####"}];
    $('#phone').inputmask({ 
        mask: phones, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });

    $("#formEmpleado").validate({
        rules: {
            first_name: {
                required: true
            },
            middle_name: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            birth_date: {
                required: true
            },
            gender: {
                required: true
            },
            street: {
                required: true
            },
            external_number: {
                required: true
            },
            neighborhood: {
                required: true
            },
            municipality: {
                required: true
            },
            city: {
                required: true
            },
            postal_code: {
                required: true,
                digits:true,
                minlength: 5,
                maxlength: 5
            },
            department_id: {
                required: true
            },
            rol_name: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Nombre (s) requerido"
            },
            middle_name: {
                required: "Ap. Paterno requerido"
            },
            phone: {
				required: "Ap. Materno requerido"
            },
            email: {
                required: "Email requerido",
                email: "Formato: abc@domain.com"
            },
            birth_date: {
                required: "Fecha Nac. requerido"
            },
            gender: {
                required: "Genero requerido"
            },
            street: {
                required: "Calle requerido"
            },
            external_number: {
                required: "Num. Ext. requerido"
            },
            neighborhood: {
                required: "Colonia requerido"
            },
            municipality: {
                required: "Municipio requerido"
            },
            city: {
                required: "Ciudad requerido"
            },
            postal_code: {
                required: "C.P. requerido",
                digits: "Solo números",
                minlength: "5 Digitos",
                maxlength: "5 Digitos"
            },
            department_id: {
				required: "Departamento requerido"
            },
            rol_name: {
                required: "Rol requerido"
            }
        },
        submitHandler: function(form) {
           form.submit();
        }
    });
});
</script>

<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Nuevo empleado</h4>
    </div>
</div>
<form method="post" id="formEmpleado"  action="{{ route('empleado.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row offset-lg-1">
        <div class="form-group col-md-4">
            <label for="first_name">Nombre (s)</label>
            <input type="text" placeholder="Nombre (s)" name="first_name" class="form-control ">
        </div>
        <div class="form-group col-md-3">
            <label for="middle_name">Apellido Paterno</label>
            <input type="text" placeholder="Apellido paterno" name="middle_name" class="form-control "
                value="{{old('nombreDos')}}">
        </div>
        <div class="form-group col-md-3">
            <label for="last_name">Apellido Materno</label>
            <input type="text" placeholder="Apellido materno" name="last_name" class="form-control ">
        </div>
    </div>

    <div class="row offset-lg-1">
        <div class="form-group col-md-3">
            <label for="phone">Teléfono</label>
            <input type="text" placeholder="Telefono" id="phone" name="phone" class="form-control ">
        </div>
        <div class="form-group col-md-3">
            <label for="email">Email</label>
            <input type="email" placeholder="Email" name="email" class="form-control ">
        </div>
        <div class="form-group col-md-2">
            <label for="birth_date">Fecha de nacimiento</label>
            <input type="date" placeholder="Fecha de nacimiento" name="birth_date" class="form-control ">
        </div>
        <div class="form-group col-md-2">
            <label for="gender">Genero</label>
            <select class="form-control" name="gender">
            <option value="">--Selecciona un genero--</option>
                <option value="Hombre">Hombre</option>
                <option value="Mujer">Mujer</option>
            </select>
        </div>

    </div>

    <div class="row  offset-lg-1">
        <div class="form-group col-md-10">
            <label for="street">Calle</label>
            <input type="text" placeholder="Calle" name="street" class="form-control ">
        </div>
    </div>

    <div class="row  offset-lg-1">
        <div class="form-group col-md-2">
            <label for="external_number">N° Exterior</label>
            <input type="number" placeholder="Numero Ext" name="external_number" class="form-control ">
        </div>
        <div class="form-group col-md-2">
            <label for="internal_number">N° Interior</label>
            <input type="number" placeholder="Numero Int" name="internal_number" class="form-control ">
        </div>

        <div class="form-group col-md-6">
            <label for="neighborhood">Colonia</label>
            <input type="text" placeholder="Colonia" name="neighborhood" class="form-control ">
        </div>
    </div>

    <div class="row  offset-lg-1">
        <div class="form-group col-md-5">
            <label for="municipality">Municipio</label>
            <input type="text" placeholder="Municipio" name="municipality" class="form-control ">
        </div>
        <div class="form-group col-md-3">
            <label for="city">Ciudad</label>
            <input type="text" placeholder="Ciudad" name="city" class="form-control ">
        </div>
        <div class="form-group col-md-2">
            <label for="postal_code">Código Postal</label>
            <input type="text" placeholder="Código postal" name="postal_code" class="form-control ">
        </div>
    </div>

    <div class="row offset-lg-1">        
        <div class="form-group col-md-1">
            <label>Fotografia</label>
            <div class="profile-upload">
                <div class="upload-img">
                    <img alt="" id="imgAvatar" src="{{url('img/usuario.png')}}" class="img-thumbnail">
                </div>
            </div>
        </div>
        <div class="form-group col-md-3">            
            <div class="form-group">
<<<<<<< HEAD
                <label for="exampleFormControlFile1">URL</label>
                <input type="file" class="form-control-file" name="avatar" id="avatar">
            </div>            
=======
                <div class="upload-input">
                    <input type="file" accept=".png, .jpg" class="form-control" name="avatar" id="avatar">
                </div>
            </div>
>>>>>>> 816366733e5361b04df15a3436e0da6340cd75a2
        </div>

        <div class="form-group col-md-3">
            <label for="department">Departamento</label>
            <select class="form-control" name="department_id">
                <option value="">-- Seleccione un departamento --</option>
                @foreach($departamentos as $k_department)
                <option value="{{$k_department->id}}">{{$k_department->department}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-3">
            <label for="rol">Rol</label>
            <select class="form-control" name="rol_name">
                <option value="">-- Seleccione un rol --</option>
                @foreach($roles as $k_rol)
                <option value="{{$k_rol->id}}">{{$k_rol->rol}}</option>
                @endforeach
            </select>
        </div>


    </div>
    <div class="row offset-lg-1">
        <div class="col-md-10 form-group">
            <label>Biografia</label>
            <textarea class="form-control" name="biography" rows="3"></textarea>
        </div>            
    </div>

    <div class="row  offset-lg-8" style="margin-top: 1%">
        <div class="col-md-3">
            <button type="submit" class="form-control btn btn-primary">Guardar</button>
        </div>
    </div>
    <br />
</form>

<script type="text/javascript">
avatar.onchange = evt => {
    const [file] = avatar.files
    if (file) {
        imgAvatar.src = URL.createObjectURL(file)
        $("#imgAvatar").attr('height', '100px');
        $("#imgAvatar").attr('width', '100px');
    }
}
</script>
@endsection
