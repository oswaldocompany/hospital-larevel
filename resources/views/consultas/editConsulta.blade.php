@extends('layouts.app')
@section('titulo','Editar Consulta')

@section('content')
	<div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Editar consulta</h3>
      </div>
    </div>
	<form method="POST" action="{{ route('consulta.update',$clave) }}" enctype="multipart/form-data">
	{{ method_field('put') }}
	@csrf

	@foreach($editConsulta as $key)
	<input type="hidden" name="patient_id" value="{{$key->patient_id}}">
		<div class="row">
			<div class="col-md-12" style="text-align: center; padding-top: 9px; font-size: 22px;">
				@foreach($nombrePaciente as $row)
				<p>Su paciente es: <strong>{{$row->first_name." ".$row->middle_name." ".$row->last_name}}</strong></p>
				@endforeach
			</div>
		</div>
 		<div class="row"> 
 			<div class="col-md-1"></div>
 			<div class="col-md-3">
 				<label for="blood_pressure">Presión Sanguinea</label>
	 			<input type="text" name="blood_pressure" value="{{$key->blood_pressure}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="temperature">Temperatura</label>
	 			<input type="text" name="temperature" value="{{$key->temperature}}" class="form-control ">
 			</div>	
	 		<div class="col-md-3">
 				<label for="weight">Peso</label>
	 			<input type="text" name="weight" value="{{$key->weight}}" class="form-control ">
 			</div>	
 			<div class="col-md-1"></div>
 		</div>

 		<div class="row">  		
 		<div class="col-md-1"></div>	
 			<div class="col-md-2">
 				<label for="stature">Estatura (cm)</label>
	 			<input type="text" name="stature" value="{{$key->stature}}" class="form-control ">
 			</div>	
 			<div class="col-md-2">
 				<label for="size">Talla (cm)</label>
	 			<input type="text" name="size" value="{{$key->size}}" class="form-control ">
 			</div>	
	 		<div class="col-md-2">
 				<label for="glucose">Glucosa</label>
	 			<input type="text" name="glucose" value="{{$key->glucose}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="saturation">Saturación</label>
	 			<input type="text" name="saturation" value="{{$key->saturation}}" class="form-control ">
 			</div>
 		<div class="col-md-1"></div>
 		</div>

 		<div class="row">
 			<div class="col-md-1"></div>
	 		<div class="col-md-5">
	 			<label for="diagnostic">Diagnostico</label>
	 			<textarea name="diagnostic" class="form-control " value="{{$key->diagnostic}}"></textarea>
	 		</div>	
	 		<div class="col-md-4">
	 			<label for="general_instructions">Instrucciones generales</label>
	 			<textarea name="general_instructions" class="form-control" value="{{$key->general_instructions}}"></textarea>
	 		</div>	
	 		<div class="col-md-1"></div>
 		</div>

 		<div class="row"> 
 			<div class="col-md-1"></div>
	 		<div class="col-md-4">
 				<label for="employee_id">Nombre del doctor</label>
	 			<select class="form-control" name="employee_id" required>
	 				<option value="">-- Seleccione quién lo atendío--</option>
	 				@foreach($doctores as $key)
	 					<option value="{{$key->id}}">{{$key->first_name.' '.$key->middle_name}}</option>
	 				@endforeach
	 			</select>
 			</div>	
 			<div class="col-md-1"></div>
 			<div class="col-md-4">
 				<label for="department_id">Área</label>
	 			<select class="form-control" name="department_id">
	 				<option value="">-- Seleccione a que departamento --</option>
	 				@foreach($areas as $key)
	 					<option value="{{$key->id}}">{{$key->department}}</option>
	 				@endforeach
	 			</select>
 			</div>	

 			{{-- <input type="hidden" name="patient_id" class="form-control" value={{$id}}> --}}
 			{{-- <div class="col-md-3">
 				<label for="ninterior">Numero interior</label>
	 			<input type="number" name="ninterior" class="form-control ">
 			</div>	 --}}
 			<div class="col-md-1"></div>
 		</div>

 		<div class="row" style="margin-top: 1%">
 			<div class="col-md-1"></div>
	 		<div class="col-md-2">
 				<button type="submit" class="form-control btn btn-primary">Actualizar</button>
 			</div>	
 		</div>
 	@endforeach
 	</form>

 	<hr>
 	<div class="col-md-12" style="text-align: center">
 		<h3>Historial de consultas del paciente</h3>
 	</div>

{{--  	<div class="row">
 		<div class="col-md-1"></div>
	 	<div class="col-md-10">
	 		@if($getConsultas == '[]')
	 			<div class="col-md-12" style="text-align: center">
	 				<p style="color:red">Aun no hay historial para este paciente</p>
	 			</div>
	 		@endif

	 		@foreach($getConsultas as $row)
	 			<p><strong>Fecha: </strong>{{\Carbon\Carbon::parse($row->created_at)->format('d/m/Y')}}</p>
	 			<p><strong>Presion Sanguinea: </strong>{{$row->blood_pressure}},<strong> Temperatura: </strong>{{$row->temperature}}, <strong>Peso: </strong>{{$row->weight}}, <strong>Glucosa: </strong>{{$row->glucose}}, <strong>Temperatura: </strong>{{$row->temperature}}, <strong>Diagnostico: </strong>{{$row->diagnostic}}, <strong>Instrucciones: </strong>{{$row->general_instructions}}</p>

	 			<hr>
	 		@endforeach
	 	</div>
	 	<div class="col-md-1"></div>
	</div> --}}
@endsection
