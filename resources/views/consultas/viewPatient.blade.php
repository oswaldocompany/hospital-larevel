@extends('layouts.app2')
@section('titulo','Buscar pacientes')

@section('content')
  {{-- @if (Auth::user()->rol == 'Administrador') --}}
    
        @include("errors.alert-danger")
        @include("errors.alert-success")
        @include("errors.alert-warning")
        @include("errors.custom-alert")

        <head>

        <script type="text/javascript">
          $(document).ready(function () {
            $(function () {
              $('#example2').DataTable({
                // "scrollY": "350px",
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language":{
                  "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
              });
           });
          });  
        </script>
      </head>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Buscar Paciente</h3>
      </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            {{-- <a href="{{url('empleados/indexEmpleado')}}" class="btn btn-success">
              Nuevo Registro
            </a> --}}
        </div>
        <div class="col-md-6">
          <h4>Seleccione un paciente para crear una consulta</h4>
        </div>
      </div>
      </br>
        {{-- <div class="row"> --}}
          <div class="col-md-12" style="padding: initial;">
            <!-- {{-- <div class="table-wrapper-scroll-y my-custom-scrollbar"> --}} -->
              <table id="example2" class="table table-bordered table-hover"> <!--table-sm-->
              <thead>
                <tr>
                  <th style="text-align: center;">Nombre</th>
                  <th style="text-align: center;">Apellidos</th>
                  <th style="text-align: center;">Telefono</th>
                  <th style="text-align: center;">E-mail</th>
                  <th style="text-align: center;">Municipio</th>
                  <th style="text-align: center;">Seleccionar</th>
                  {{-- <th style="text-align: center;">Eliminar</th> --}}
                 </tr>
              </thead>
                <tbody class="buscar">
                  @foreach($allEmployee as $valor)
                    {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
                    <tr class="table-light">
                      <td style="text-align: center;">{{$valor->first_name}}</td>
                      <td style="text-align: center;">{{$valor->middle_name." ".$valor->last_name}}</td>
                      <td style="text-align: center;">{{$valor->phone}}</td>
                      <td style="text-align: center;">{{$valor->email}}</td>
                      <td style="text-align: center;">{{$valor->neighborhood}}</td>

                      <td style="text-align: center;">
                          <a class="" href="{{url('consulta/createT/'.encrypt($valor->id))}}"><img src="img/ver.png"/></a>
                      </td>

                      {{-- <td style="text-align: center;">
                        <form action="{{ url('/cuarto/cerrarCuenta/'.$Id2) }}" method="post">
                            {{ csrf_field()}}
                            <input type="hidden" name="fecha" id="fecha" value="{{$fechaTodo}}">
                            <input type="image" src="img/cuenta.png"/>
                         </form>  
                      </td> --}}

                      {{-- <td style="text-align: center;">
                          <a class="btn btn-primary" href="{{url('/empleados/'.encrypt($valor->id).'/edit')}}"><img src="img/editar.png"/></a>
                      </td> --}}

                     {{--  <td style="text-align: center;"> 
                         <form action="{{ url('/empleados/destroy/'.$valor->id) }}" method="post" id="form_deleted_col">
                             
                           <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                            <button class="btn btn-danger botonCus" type="submit" onclick="return AlertEliminar(event,{{ $valor->id }})">
                              <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                              
                              <input class="btn btn-danger" type="image" src="img/eliminar.png"/>
                            </button> 
                        </form>     
                      </td> --}}
                       {{--<td style="text-align: center;">
                          <a class="" onclick="return AlertEliminar(event,{{$valor->id}})"><img src="img/eliminar.png"/></a>
                      </td>--}}

                    </tr>
                  @endforeach

                </tbody>
              </table>
          </div>
        </div>
{{--    @else
    <div class="row">
      <div class="col-md-4"></div>
      <div class="alert alert-danger" id="alert-danger" style="display:block">
        <a class="close" onclick="$('.alert').fadeOut()">X</a>    
        <div id="text-alert-danger">
          ¡¡ No puedes accesar a esta ruta !!...
        </div>
      </div>
    </div>
  @endif --}}
@endsection

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
