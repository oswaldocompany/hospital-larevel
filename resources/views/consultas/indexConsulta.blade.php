@extends('layouts.app2')
@section('titulo','Ver Contratados')

@section('content')
  {{-- @if (Auth::user()->rol == 'Administrador') --}}
    
        @include("errors.alert-danger")
        @include("errors.alert-success")
        @include("errors.alert-warning")
        @include("errors.custom-alert")

        <head>

        <script type="text/javascript">
          $(document).ready(function () {
            $(function () {
              $('#example2').DataTable({
                // "scrollY": "350px",
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language":{
                  "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
              });
           });
          });  
        </script>
      </head>

          {{-- <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                  <a href="{{url('usuario/create')}}" class="btn btn-success">
                    Agregar Usuario
                  </a>
              </div>
              
              <div class="col-md-4">
                <div class="input-group">
                  <span class="input-group-addon">Buscar</span>
                  <input id="filtrar" type="text" class="form-control" placeholder="Buscar">
                </div>
              </div>
            </div>
          </div> --}}
        {{-- <hr>
    </br>
    </br> --}}
      <br/>
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-5">
          <h3>Todas las consultas</h3>
        </div>
      </div>
     <div class="row">
        <div class="col-md-4">
            <a href="{{url('showPatient')}}" class="btn btn-primary">
              Nuevo Registro
            </a>
        </div>
        {{-- <div class="col-md-4">
          <h3>Registros</h3>
        </div> --}}
      </div>
      </br>
        {{-- <div class="row"> --}}
          <div class="col-md-12" style="padding: initial;">
            <!-- {{-- <div class="table-wrapper-scroll-y my-custom-scrollbar"> --}} -->
              <table id="example2" class="table table-bordered table-hover"> <!--table-sm-->
              <thead>
                <tr>
                  <th style="text-align: center;">Nombre del paciente</th>
                  {{-- <th style="text-align: center;">Nombre del doctor</th> --}}
                  <th style="text-align: center;">Departamento</th>
                  <th style="text-align: center;">Diagnostico</th>
                  <th style="text-align: center;">Instrucciones Generales</th>
                  <th style="text-align: center;">Editar</th>
                  <th style="text-align: center;">Eliminar</th>
                 </tr>
              </thead>
                <tbody class="buscar">
                  @foreach($allConsultas as $valor)
                    {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
                    <tr class="table-light">
                      <td style="text-align: center;">{{$valor->first_name." ".$valor->last_name}}</td>
                      {{-- <td style="text-align: center;">{{$valor->first_name." ".$valor->last_name}}</td> --}}
                      <td style="text-align: center;">{{$valor->department}}</td>
                      <td style="text-align: center;">{{$valor->diagnostic}}</td>
                      <td style="text-align: center;">{{$valor->general_instructions}}</td>

                      {{-- <td style="text-align: center;">
                          <?php $Id2 = $valor->codigo?>
                          <a class="" href="{{url('/cuarto/verCuenta/'.encrypt($valor->n_cuarto).'/'.encrypt($Id2))}}"><img src="img/ver.png"/></a>
                      </td> --}}

                      {{-- <td style="text-align: center;">
                        <form action="{{ url('/cuarto/cerrarCuenta/'.$Id2) }}" method="post">
                            {{ csrf_field()}}
                            <input type="hidden" name="fecha" id="fecha" value="{{$fechaTodo}}">
                            <input type="image" src="img/cuenta.png"/>
                         </form>  
                      </td> --}}

                      <td style="text-align: center;">
                          <a class="btn btn-primary" href="{{url('/consulta/'.encrypt($valor->id).'/edit')}}"><img src="img/editar.png"/></a>
                      </td>

                      <td style="text-align: center;"> 
                         <form action="{{ route('consulta.inactivar',$valor->id) }}" method="DELETE" id="form_deleted_col">
                             
                           <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                            <button class="btn btn-danger botonCus" type="submit" onclick="return AlertEliminar(event,{{ $valor->id }})">
                              <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                              
                              <input class="btn btn-danger" type="image" src="img/eliminar.png"/>
                            </button> 
                        </form>     
                      </td>
                       {{--<td style="text-align: center;">
                          <a class="" onclick="return AlertEliminar(event,{{$valor->id}})"><img src="img/eliminar.png"/></a>
                      </td>--}}

                    </tr>
                  @endforeach

                </tbody>
              </table>
            {{-- </div> Div Scroll custom --}}
          </div>
         {{-- </div> --}}
         {{-- <div class="col-md-4">
                {{ $datos->links() }} Con esto maás la paignacion del controller vamos a poder crear una paginacion
          </div> --}}
        </div>
{{--    @else
    <div class="row">
      <div class="col-md-4"></div>
      <div class="alert alert-danger" id="alert-danger" style="display:block">
        <a class="close" onclick="$('.alert').fadeOut()">X</a>    
        <div id="text-alert-danger">
          ¡¡ No puedes accesar a esta ruta !!...
        </div>
      </div>
    </div>
  @endif --}}
@endsection

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

 <script type="text/javascript">

    function AlertEliminar(event, id) {
        event.preventDefault();
        // var route = BASE_URL + "cuarto/borrar/" + id;
        var route = BASE_URL + "consulta/inactivar/" + id;
        // alert (route);
        swal({
          title: "¿Está seguro de eliminar este registro?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, Eliminalo!",
          closeOnConfirm: false
      }
      , function (inputValue) {
          if (inputValue === false) return false;
        
          // var form = $("#form_deleted_col").attr("action", route);
          // $("#form_deleted_col").submit();
          window.location = route; 
            swal("Correcto!", "Se elimino correctamente", "success");
      }
      );   
    }
</script>