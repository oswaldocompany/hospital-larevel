@extends('layouts.app')
@section('titulo','Editar Paciente')

@section('content')
	<div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Editar paciente</h3>
      </div>
    </div>
	<form method="post" action="{{ route('paciente.update',$clave) }}" enctype="multipart/form-data">
		{{ method_field('put') }}
		@csrf

		@foreach($editPatient as $key)
		{{-- <input type="hidden" name="company_id" value="{{$id}}"> --}}
 		<div class="row"> 
 			<div class="col-md-1"></div>
 			<div class="col-md-5">
 				<label for="first_name">Nombre</label>
	 			<input type="text" name="first_name" value="{{$key->first_name}}" class="form-control ">
 			</div>	
 			<div class="col-md-5">
 				<label for="middle_name">Apellido Paterno</label>
	 			<input type="text" name="middle_name" value="{{$key->middle_name}}" class="form-control ">
 			</div>
 			<div class="col-md-1"></div>	
	 		
 		</div>

 		<div class="row"> 
 			<div class="col-md-1"></div>
 			<div class="col-md-4">
 				<label for="last_name">Apellido Materno</label>
	 			<input type="text" name="last_name" value="{{$key->last_name}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="phone">Teléfono</label>
	 			<input type="phone" name="phone" value="{{$key->phone}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="email">Email</label>
	 			<input type="email" name="email" value="{{$key->email}}" class="form-control ">
 			</div>	

 		</div>

 		<div class="row"> 
 			<div class="col-md-1"></div>
	 		<div class="col-md-3">
 				<label for="birth_date">Fecha de cumpleaños</label>
	 			<input type="date" name="birth_date" value="{{$key->birth_date}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="gender">Genero</label>
	 			<select class="form-control" name="gender">
	 				<option value="hombre">Hombre</option>
	 				<option value="mujer">Mujer</option>
	 				<option value="otro">Sin especificar</option>
	 			</select>
 			</div>	
 			<div class="col-md-4">
 				<label for="street">Calle</label>
	 			<input type="text" name="street" value="{{$key->street}}" class="form-control ">
 			</div>	
 			<div class="col-md-1"></div>

 		</div>

 		<div class="row">
 			<div class="col-md-1"></div>
	 		<div class="col-md-2">
 				<label for="external_number">Numero exterior</label>
	 			<input type="number" name="external_number" value="{{$key->external_number}}" class="form-control ">
 			</div>	
 			<div class="col-md-2">
 				<label for="internal_number">Numero interior</label>
	 			<input type="number" name="internal_number" value="{{$key->internal_number}}" class="form-control ">
 			</div>	

 			<div class="col-md-3">
 				<label for="neighborhood">Colonia</label>
	 			<input type="text" name="neighborhood" value="{{$key->neighborhood}}" class="form-control ">
 			</div>	
	 		<div class="col-md-3">
 				<label for="municipality">Municipio</label>
	 			<input type="text" name="municipality" value="{{$key->municipality}}" class="form-control ">
 			</div>	
 			
 		</div>

 		<div class="row">
 			<div class="col-md-1"></div>
 			{{-- <div class="col-md-4">
	 			<label for="avatar">Foto de perfil</label>
	 			<input type="file" name="avatar">
	 		</div> --}}
	 		<div class="col-md-3">
 				<label for="city">Ciudad</label>
	 			<input type="text" name="city" value="{{$key->city}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="postal_code">Código Postal</label>
	 			<input type="text" name="postal_code" value="{{$key->postal_code}}" class="form-control ">
 			</div>	

	 		<div class="col-md-4">
	 			<label for="descripcion">Comentarios</label>
	 			<textarea name="descripcion" class="form-control">{{$key->descripcion}}</textarea>
	 		</div>	
 		</div>

 		<div class="row" style="margin-top: 1%">
 			<div class="col-md-1"></div>
	 		<div class="col-md-2">
 				<button type="submit" class="form-control btn btn-primary">Actualizar</button>
 			</div>	
 		</div>

 	@endforeach
 	</form>
@endsection
