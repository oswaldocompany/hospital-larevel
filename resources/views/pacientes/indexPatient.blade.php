@extends('layouts.app2')
@section('titulo','Ver Pacientes')

@section('content')
{{-- @if (Auth::user()->rol == 'Administrador') --}}

@include("errors.alert-danger")
@include("errors.alert-success")
@include("errors.alert-warning")
@include("errors.custom-alert")

<head>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
</head>

<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Pacientes</h4>
    </div>
</div>

<div class="row offset-lg-1">
    <div class="col-md-4">
        <a href="{{url('paciente/create')}}" class="btn btn-primary">
            Nuevo Registro
        </a>
    </div>
</div>

<div class="col-md-10 offset-lg-1" style="padding: initial;">
    <!-- {{-- <div class="table-wrapper-scroll-y my-custom-scrollbar"> --}} -->
    <table id="example2" class="table table-bordered table-hover">
        <!--table-sm-->
        <thead>
            <tr>
                <th style="text-align: center;">Fotografia</th>
                <th style="text-align: center;">Nombre(s)</th>
                <th style="text-align: center;">Apellido paterno</th>
                <th style="text-align: center;">Apellido materno</th>
                <th style="text-align: center;">Telefono</th>
                <th style="text-align: center;">E-mail</th>
                <th style="text-align: center;">Fecha de nacimiento</th>
                <th style="text-align: center;">Municipio</th>
                <th style="text-align: center;">Editar</th>
                <th style="text-align: center;">Eliminar</th>
            </tr>
        </thead>
        <tbody class="buscar">
            @foreach($allPatient as $valor)
            {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
            <tr class="table-light">
                <td style="text-align: center;">
                    @if($valor->avatar == null ||$valor->avatar =='' )
                    <input type="image" style="width:40px; height:45px;" src="{{url('img/usuario.png')}}" />
                    @else
                    <input type="image" style="width:40px; height:45px;" src="{{asset('storage/'.$valor->avatar)}}" />
                    @endif
                </td>
                <td style="text-align: center;">{{$valor->first_name}}</td>
                <td style="text-align: center;">{{$valor->middle_name}}</td>
                <td style="text-align: center;">{{$valor->last_name}}</td>
                <td style="text-align: center;">{{$valor->phone}}</td>
                <td style="text-align: center;">{{$valor->email}}</td>
                <td style="text-align: center;">{{$valor->birth_date}}</td>
                <td style="text-align: center;">{{$valor->neighborhood}}</td>

                <td style="text-align: center;">
                    <a class="btn btn-primary" href="{{url('/paciente/'.encrypt($valor->id).'/edit')}}"><i
                            class="far fa-edit"></i></a>
                </td>

                <td style="text-align: center;">
                    <form action=" {{ route('paciente.inactivar',$valor->id) }} " method="delete" id="form_deleted_col">

                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                        <button class="btn btn-danger botonCus" type="submit"
                            onclick="return AlertEliminar(event,{{ $valor->id }})">
                            <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </form>
                </td>

            </tr>
            @endforeach

        </tbody>
    </table>

</div>

</div>

@endsection

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
    $('#example2').DataTable();

    function AlertEliminar(event, id) {
        event.preventDefault();        
        var route = BASE_URL + "empleado/inactivar/" + id;
        swal({
            title: "¿Está seguro de eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, Eliminalo!",
            closeOnConfirm: false
        }, function(inputValue) {
            if (inputValue === false) return false;
            window.location = route;
            swal("Correcto!", "Se elimino correctamente", "success");
        });
    }
});
</script>