@extends('layouts.app')
@section('titulo','Nuevo Paciente')

@section('content')
<style>
.error {
  color: #af5157;
}
</style>
<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">
$(document).ready(function() {    
    var phones = [{ "mask": "(###) ###-####"}];
    $('#phone').inputmask({ 
        mask: phones, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });

    $("#formPatient").validate({
        rules: {
            first_name: {
                required: true
            },
            middle_name: {
                required: true
            },
            birth_date: {
                required: true
            },
            gender: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Nombre (s) requerido"
            },
            middle_name: {
                required: "Ap. Paterno requerido"
            },
            birth_date: {
                required: "Fecha Nac. requerido"
            },
            gender: {
                required: "Genero requerido"
            }
        },
        submitHandler: function(form) {
           form.submit();
        }
    });
});
</script>

<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Agregar paciente</h4>
    </div>
</div>
<form method="post" id="formPatient" action="{{ route('paciente.store') }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="company_id" value="{{$id}}">

    <div class="row offset-lg-1">
        <div class="col-md-3">
            <label for="first_name">Nombre</label>
            <input type="text" name="first_name" class="form-control" placeholder="Nombres(s)" autocomplete="off">
        </div>
        <div class="col-md-3">
            <label for="middle_name">Apellido Paterno</label>
            <input type="text"  name="middle_name" class="form-control" placeholder="Apellido paterno" autocomplete="off">
        </div>
        <div class="col-md-3">
            <label for="last_name">Apellido Materno</label>
            <input type="text"  name="last_name" class="form-control" placeholder="Apellido materno" autocomplete="off">
        </div>
    </div>

    <div class="row offset-lg-1">
        <div class="col-md-2">
            <label for="phone">Teléfono</label>
            <input type="text" id="phone" name="phone" placeholder="Telefono" class="form-control" autocomplete="off">
        </div>
        <div class="col-md-3">
            <label for="email">Email</label>
            <input type="text" name="email" placeholder="E-mail" class="form-control" autocomplete="off">
        </div>
        <div class="col-md-2">
            <label for="birth_date">Fecha de nacimiento</label>
            <input type="date" placeholder="Fecha de nacimiento" name="birth_date" class="form-control" autocomplete="off">
        </div>
        <div class="col-md-2">
            <label for="gender">Genero</label>
            <select class="form-control" name="gender">
                <option value="">--Seleccionar--</option>
                <option value="Hombre">Hombre</option>
                <option value="Mujer">Mujer</option>            
            </select>
        </div>
    </div>

    <div class="row offset-lg-1">

        <div class="col-md-3">
            <label for="street">Calle</label>
            <input type="text" placeholder="Calle" name="street" placeholder="Calle" class="form-control" autocomplete="off">
        </div>
        <div class="col-md-2">
            <label for="external_number">Numero exterior</label>
            <input type="number" placeholder="Número Ext." name="external_number" placeholder="Num. Exterior" class="form-control ">
        </div>
        <div class="col-md-2">
            <label for="internal_number">Numero interior</label>
            <input type="number" placeholder="Número Int." name="internal_number"  class="form-control ">
        </div>
        <div class="col-md-2">
            <label for="neighborhood">Código Postal</label>
            <input type="text"  name="postal_code" placeholder="Codigo postal" class="form-control ">
        </div>
    </div>

    <div class="row offset-lg-1">
        <div class="col-md-3">
            <label for="postal_code">Colonia</label>
            <input type="text" name="neighborhood" placeholder="Colonia" class="form-control ">
        </div>
        <div class="col-md-3">
            <label for="municipality">Municipio</label>
            <input type="text" name="municipality" placeholder="Municipio" class="form-control ">
        </div>
        <div class="col-md-3">
            <label for="city">Ciudad</label>
            <input type="text" name="city" placeholder="Ciudad" class="form-control ">
        </div>
    </div>

    <div class="row offset-lg-1">        
        <div class="form-group col-md-1">
            <label>Fotografia</label>
            <div class="profile-upload">
                <div class="upload-img">
                    <img alt="" id="imgAvatar" src="{{url('img/usuario.png')}}" class="img-thumbnail">
                </div>
            </div>
        </div>
        <div class="form-group col-md-3">            
            <div class="form-group">
                <label for="exampleFormControlFile1">URL</label>
                <input type="file" class="form-control-file" name="avatar" id="avatar">
            </div>            
        </div>

        <div class="col-md-5">
            <p for="descripcion">Antecedente clinico del paciente</label>
            <textarea name="descripcion" class="form-control "></textarea>
        </div>
    </div>

    <div class="row  offset-lg-1">
        <div class="col-md-2">
            <button type="submit" class="form-control btn btn-primary">Guardar</button>
        </div>
    </div>
</form>
<script type="text/javascript">
avatar.onchange = evt => {
    const [file] = avatar.files
    if (file) {
        imgAvatar.src = URL.createObjectURL(file)        
    }
}
</script>
@endsection