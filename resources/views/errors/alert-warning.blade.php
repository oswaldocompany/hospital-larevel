@if(Session::has("warning"))
	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
		</a>
		{!! Session::get("warning") !!}
	</div>
@endif
<div class="alert alert-warning" id="alert-warning" style="display:none">
	<a class="close" onclick="$('.alert').fadeOut()">×</a>    
	<div id="text-alert-warning">
	</div>
</div>