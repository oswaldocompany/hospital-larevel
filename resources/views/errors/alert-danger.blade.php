@if(count($errors) > 0)
<div class="alert alert-danger">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
</a>
@foreach($errors->all() as $error)
	*{{ $error }}<br />
@endforeach
</div>
@endif
@if(Session::has("danger"))
<div class="alert alert-danger">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
	</a>
	{{ Session::get("danger") }}
</div>
@endif
<div class="alert alert-danger" id="alert-danger" style="display:none">
	<a class="close" onclick="$('.alert').fadeOut()">×</a>    
	<div id="text-alert-danger">
	</div>
</div>