@if(Session::has("success"))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
		</a>
		{!! Session::get("success") !!}
	</div>
@endif
<div class="alert alert-success" id="alert-success" style="display:none">
	<a class="close" onclick="$('.alert').fadeOut()">×</a>    
	
		<div class="modal fade" id="create">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		            	<h4>Buscar producto</h4>
		                <button type="button" class="close" data-dismiss="modal">
		                    <span>×</span>
		                </button>
		                
		            </div>
		            <div class="modal-body">
		                {{-- <div id="text-alert-success"></div> --}}
		            </div>
		            <div class="modal-footer">
		                <input type="submit" class="btn btn-primary" value="Guardar">
		            </div>
		        </div>
		    </div>
		</div>
	
</div>