{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> --}}
@if(Session::has("mensaje-custom-alert2"))
	<div class="alert alert-succes">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;
		</a>
		{{-- {!! Session::get("mensaje-custom-alert2") !!} --}}
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript">
			swal("El total de la cuenta fue", "{!! Session::get("mensaje-custom-alert2") !!}","success");
		</script>
	</div>
@endif
