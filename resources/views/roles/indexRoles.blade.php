@extends('layouts.app')
@section('titulo','Ver Roles')

@section('content')
  {{-- @if (Auth::user()->rol == 'Administrador') --}}
    
        @include("errors.alert-danger")
        @include("errors.alert-success")
        @include("errors.alert-warning")
        @include("errors.custom-alert")

        <head>

        <script type="text/javascript">
          $(document).ready(function () {
            $(function () {
              $('#example2').DataTable({
                // "scrollY": "350px",
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language":{
                  "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
              });
           });
          });  
        </script>
      </head>

          {{-- <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                  <a href="{{url('usuario/create')}}" class="btn btn-success">
                    Agregar Usuario
                  </a>
              </div>
              
              <div class="col-md-4">
                <div class="input-group">
                  <span class="input-group-addon">Buscar</span>
                  <input id="filtrar" type="text" class="form-control" placeholder="Buscar">
                </div>
              </div>
            </div>
          </div> --}}
        {{-- <hr>
    </br>
    </br> --}}
    
     <div class="row">
        <div class="col-md-4">
          {{-- <h3>Registros</h3> --}}
        </div>
      </div>
      </br>
        {{-- <div class="row"> --}}
          <div class="col-md-12" style="padding: initial;">
            <!-- {{-- <div class="table-wrapper-scroll-y my-custom-scrollbar"> --}} -->
              <table id="example2" class="table table-bordered table-hover"> <!--table-sm-->
              <thead>
                <tr>
                  <th style="text-align: center;">Rol</th>
                  <th style="text-align: center;">Estado</th>
                 </tr>
              </thead>
                <tbody class="buscar">
                  @foreach($allRoles as $valor)
                    {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
                    <tr class="table-light">
                      <td style="text-align: center;">{{$valor->rol}}</td>
                      <td style="text-align: center;">{{$valor->status}}</td>
                      

                      {{-- <td style="text-align: center;">
                          <a class="btn btn-primary" href="{{url('/areas/'.encrypt($valor->id).'/edit')}}"><img src="img/editar.png"/></a>
                      </td> --}}

                      {{-- <td style="text-align: center; padding-bottom: 0%"> 
                         <form action="{{ url('/areas/destroy/'.$valor->id) }}" method="post" id="form_deleted_col">
                             
                           <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                            <button class="btn btn-danger botonCus"  type="submit" onclick="return AlertEliminar(event,{{ $valor->id }})">
                              <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                              
                              <input class="btn btn-danger" type="image" src="img/eliminar.png"/>
                            </button> 
                        </form>     
                      </td> --}}
                       {{--<td style="text-align: center;">
                          <a class="" onclick="return AlertEliminar(event,{{$valor->id}})"><img src="img/eliminar.png"/></a>
                      </td>--}}

                    </tr>
                  @endforeach

                </tbody>
              </table>

          </div>
        </div>
{{--    @else
    <div class="row">
      <div class="col-md-4"></div>
      <div class="alert alert-danger" id="alert-danger" style="display:block">
        <a class="close" onclick="$('.alert').fadeOut()">X</a>    
        <div id="text-alert-danger">
          ¡¡ No puedes accesar a esta ruta !!...
        </div>
      </div>
    </div>
  @endif --}}
@endsection

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 {{-- <script src="{{url('js/jquery.dataTables.js')}}"></script> --}}
 <script type="text/javascript">
  function AlertEliminar(event, id) {
      event.preventDefault();
      // var route = BASE_URL + "cuarto/borrar/" + id;
      var route = BASE_URL + "areas/borrar/" + id;
      // alert (route);
      swal({
        title: "¿Desea eliminar los datos guardados de esta habitación?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
      confirmButtonText: "Si, Eliminalo!",
        closeOnConfirm: false
    }
    , function (inputValue) {
        if (inputValue === false) return false;
      
        // var form = $("#form_deleted_col").attr("action", route);
        // $("#form_deleted_col").submit();
        window.location = route; 
          swal("Correcto!", "Se elimino correctamente", "success");
    }
    );   
  }
</script>