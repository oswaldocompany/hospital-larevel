@extends('layouts.app')
{{-- @section('titulo','Soporte') --}}

<!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  {{-- <link rel="stylesheet" href="../dist/css/adminlte.min.css"> --}}

@section('content')
	<div class="col-sm-8">
	  <h1 class="m-0">Contáctanos</h1>
    <div class="row">
          {{-- <h5 class="mb-2">Datos</h5> --}}
    </div>
	</div><!-- /.col -->
	<br>

	

  <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Email</span>
                <span class="info-box-number">soporte@hospital.com</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Otro</span>
                <span class="info-box-number">Otro card de datos</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          {{-- <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Uploads</span>
                <span class="info-box-number">13,648</span>
              </div>
            
            </div> --}}
           
          </div>
          <!-- /.col -->
          {{-- <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Likes</span>
                <span class="info-box-number">93,139</span>
              </div>
            </div>
          </div> --}}
          <!-- /.col -->
        {{-- </div> --}}
	
@endsection