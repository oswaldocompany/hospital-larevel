@extends('layouts.app')
{{-- @section('titulo','Editar Departamento') --}}

@section('content')

<style>
form .error {
    color: #ff0000;
}
</style>
<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#formArea").validate({
        rules: {
            department: {
                required: true
            }
        },
        messages: {
            department: {
                required: "El nombre del departamento es obligatorio",
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});
</script>

<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Editar área</h4>
    </div>
</div>

<form method="post" id="formArea" action="{{ route('area.update',$clave) }}" enctype="multipart/form-data">
    {{ method_field('put') }}
    @csrf
    {{-- <input type="text" name="company_id" value="{{$id}}"> --}}
    <div class="row offset-lg-1">
        @foreach($editArea as $key)
        <div class="col-md-5">
            <p for="department">Nombre del departamento</p>
            <input type="text" name="department" placeholder="Ejemplo: Pediatria" value="{{$key->department}}" class="form-control">
        </div>
        <div class="col-md-5">
            <p for="department">Estado</p>
            <select name="status" class="form-control" required>
                <option value="ACTIVE">Activar</option>
                <option value="INAACTIVE">Desactivar</option>
            </select>
        </div>
        @endforeach
    </div>

    <div class="row offset-lg-1" style="margin-top: 1%">        
        <div class="col-md-2">
            <button type="submit" class="form-control btn btn-primary">Actualizar</button>
        </div>
    </div>
</form>
@endsection