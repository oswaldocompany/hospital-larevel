@extends('layouts.app2')
@section('titulo','Ver Áreas')

@section('content')
{{-- @if (Auth::user()->rol == 'Administrador') --}}

@include("errors.alert-danger")
@include("errors.alert-success")
@include("errors.alert-warning")
@include("errors.custom-alert")

<head>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    <script type="text/javascript">
    $(document).ready(function() {
        $(function() {
            $('#example2').DataTable({
                // "scrollY": "350px",
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
            });
        });
    });
    </script>
</head>

{{-- <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                  <a href="{{url('usuario/create')}}" class="btn btn-success">
Agregar Usuario
</a>
</div>

<div class="col-md-4">
    <div class="input-group">
        <span class="input-group-addon">Buscar</span>
        <input id="filtrar" type="text" class="form-control" placeholder="Buscar">
    </div>
</div>
</div>
</div> --}}
{{-- <hr>
    </br>
    </br> --}}
<br />
<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Áreas</h4>
    </div>
</div>
<div class="row offset-lg-1">
    <div class="col-md-4">
        <a href="{{url('area/create')}}" class="btn btn-primary">
            Nuevo Registro
        </a>
    </div>
    <div class="col-md-4">
        {{-- <h3>Registros</h3> --}}
    </div>
</div>
</br>
{{-- <div class="row"> --}}
<div class="col-md-10 offset-lg-1" style="padding: initial;">
    <!-- {{-- <div class="table-wrapper-scroll-y my-custom-scrollbar"> --}} -->
    <table id="example2" class="table table-striped">
        <!--table-sm-->
        <thead>
            <tr>
                <th style="text-align: center;">Departamento</th>
                <th style="text-align: center;">Estado</th>
                <th style="text-align: center;">Editar</th>
                <th style="text-align: center;">Eliminar</th>
            </tr>
        </thead>
        <tbody class="buscar">
            @foreach($allAreas as $valor)
            {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
            <tr class="table-light">
                <td style="text-align: center;">{{$valor->department}}</td>
                <td style="text-align: center;">{{$valor->status}}</td>

                {{-- <td style="text-align: center;">
                          <?php $Id2 = $valor->codigo?>
                          <a class="" href="{{url('/cuarto/verCuenta/'.encrypt($valor->n_cuarto).'/'.encrypt($Id2))}}"><img
                    src="img/ver.png" /></a>
                </td> --}}

                {{-- <td style="text-align: center;">
                        <form action="{{ url('/cuarto/cerrarCuenta/'.$Id2) }}" method="post">
                {{ csrf_field()}}
                <input type="hidden" name="fecha" id="fecha" value="{{$fechaTodo}}">
                <input type="image" src="img/cuenta.png" />
                </form>
                </td> --}}

                {{-- <td>
                          <a href="" id="editArea" data-toggle="modal" data-target='#practice_modal' data-id="{{ $valor->id }}">Edit</a>
                </td> --}}


                <td style="text-align: center;">
                    <a class="btn btn-primary" href="{{url('/area/'.encrypt($valor->id).'/edit')}}"><img
                            src="img/editar.png" /></a>
                </td>

                <td style="text-align: center; padding-bottom: 0%">
                    <form action="{{ url('/area/inactivar/'.$valor->id) }}" method="post" id="form_deleted_col">

                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">
                        <button class="btn btn-danger botonCus" type="submit"
                            onclick="return AlertEliminar(event,{{ $valor->id }})">
                            <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->id}}">

                            <input class="btn btn-danger" type="image" src="img/eliminar.png" />
                        </button>
                    </form>
                </td>
                {{--<td style="text-align: center;">
                          <a class="" onclick="return AlertEliminar(event,{{$valor->id}})"><img
                    src="img/eliminar.png" /></a>
                </td>--}}

            </tr>
            @endforeach

        </tbody>
    </table>
    {{-- </div> Div Scroll custom --}}

    <div class="modal fade" id="practice_modal">
        <div class="modal-dialog">
            <form id="companydata">
                <div class="modal-content">
                    <input type="hidden" id="id_eliminar" name="id_eliminar" value="">
                    <div class="modal-body">
                        <label for="name">Editar elemento</label>
                        <input type="text" name="name" id="name" value="" class="form-control">
                    </div>
                    <input type="submit" value="Submit" id="submit" class="btn btn-sm btn-outline-danger py-0"
                        style="font-size: 0.8em;">
                </div>
            </form>
        </div>
    </div>


</div>
{{-- </div> --}}
{{-- <div class="col-md-4">
                {{ $datos->links() }} Con esto maás la paignacion del controller vamos a poder crear una paginacion
</div> --}}
</div>
{{--    @else
    <div class="row">
      <div class="col-md-4"></div>
      <div class="alert alert-danger" id="alert-danger" style="display:block">
        <a class="close" onclick="$('.alert').fadeOut()">X</a>    
        <div id="text-alert-danger">
          ¡¡ No puedes accesar a esta ruta !!...
        </div>
      </div>
    </div>
  @endif --}}
@endsection

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
// MODAL
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('body').on('click', '#submit', function(event) {
    alert("Submit");
    event.preventDefault()
    var id = $("#id_eliminar").val();
    var name = $("#name").val();

    $.ajax({
        url: 'areas/' + id,
        type: "POST",
        data: {
            id: id,
            name: name,
        },
        dataType: 'json',
        success: function(data) {
            $('#companydata').trigger("reset");
            $('#practice_modal').modal('hide');
            window.location.reload(true);
        }
    });
});

$('#editArea').click(function(event) {
    event.preventDefault();
    var id = $(this).data('id');
    console.log("id- " + id);
    $.get('areas/' + id + '/edit', function(data) {
        $('#userCrudModal').html("Edit category");
        $('#submit').val("Edit category");
        $('#practice_modal').modal('show');
        $('#id_eliminar').val(data.data.id);
        $('#name').val(data.data.name);
    })
});


function AlertEliminar(event, id) {
    event.preventDefault();    
    var route = BASE_URL + "area/inactivar/" + id;    
    swal({
        title: "¿Esta seguro que desea eliminar este registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, Eliminalo!",
        closeOnConfirm: false
    }, function(inputValue) {
        if (inputValue === false) return false;        
        window.location = route;
        swal("Correcto!", "Se elimino correctamente", "success");
    });
}
</script>