@extends('layouts.app')
@section('titulo','Nuevo Departamento')

@section('content')
<style>
form .error {
    color: #ff0000;
}
</style>
<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#formArea").validate({
        rules: {
            department: {
                required: true
            }
        },
        messages: {
            department: {
                required: "Departamento es obligatorio",
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});
</script>

<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Nueva área</h4>
    </div>
</div>

<form method="post" id="formArea" action="{{ route('area.store') }}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="company_id" value="{{$id}}">
    <div class="row offset-lg-1">
        <div class="col-md-6">
            <p for="department">Nombre del departamento</p>
            <input type="text" placeholder="Ejemplo: Pediatria" name="department" class="form-control">
        </div>
    </div>

    <div class="row offset-lg-1" style="margin-top: 1%">
        <div class="col-md-2">
            <button type="submit" class="form-control btn btn-primary">Guardar</button>
        </div>
    </div>
</form>
@endsection