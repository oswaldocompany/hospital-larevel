@extends('layouts.app')
@section('titulo','Nueva Cita')

@section('content')
	<div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Agregar nueva cita</h3>
      </div>
    </div>
	<form method="POST" action="{{ route('cita.store') }}" enctype="multipart/form-data">
		@csrf
 		<div class="row"> 
 			<div class="col-md-1"></div>
 			<div class="col-md-3">
 				<label for="patient_id">Paciente</label>
	 			<select class="form-control" name="patient_id" required>
	 				<option value="">-- Seleccione un paciente --</option>
	 				@foreach($pacientes as $key)
	 					<option value="{{$key->id}}">{{$key->first_name .' '.$key->middle_name .' '. $key->last_name}}</option>
	 				@endforeach
	 			</select>
 			</div>	
 			<div class="col-md-3">
 				<label for="employee_id">Nombre del Doctor</label>
	 			<select class="form-control" name="employee_id" required>
	 				<option value="">-- Seleccione quién lo atenderá --</option>
	 				@foreach($doctores as $key)
	 					<option value="{{$key->id}}">{{$key->first_name.' '.$key->middle_name}}</option>
	 				@endforeach
	 			</select>
 			</div>	
 			<div class="col-md-3">
 				<label for="department_id">Área</label>
	 			<select class="form-control" name="department_id">
	 				<option value="">-- Seleccione a que departamento --</option>
	 				@foreach($areas as $key)
	 					<option value="{{$key->id}}">{{$key->department}}</option>
	 				@endforeach
	 			</select>
 			</div>
 			<div class="col-md-1"></div>	
 		</div>

 		<div class="row">  	
 		<div class="col-md-1"></div>		
 			<div class="col-md-2">
 				<label for="date_appointment">Fecha de Cita</label>
	 			<input type="date" name="date_appointment" class="form-control ">
 			</div>	
 			<div class="col-md-1">
 				<label for="time_start">Hora</label>
	 			<input type="datetime" name="time_start" class="form-control ">
 			</div>	
	 		<div class="col-md-3">
 				<label for="patient_email">Correo del paciente</label>
	 			<input type="text" name="patient_email" class="form-control " required>
 			</div>	
 			<div class="col-md-3">
 				<label for="patient_number">Número del paciente</label>
	 			<input type="phone" name="patient_number" class="form-control ">
 			</div>
 			<div class="col-md-1"></div>
 		</div>

 		<div class="row">
 			<div class="col-md-1"></div>
	 		<div class="col-md-9">
	 			<label for="message">Mensaje</label>
	 			<textarea name="message" class="form-control "></textarea>
	 		</div>	
 		</div>

 		<div class="row" style="margin-top: 1%">
 			<div class="col-md-1"></div>
	 		<div class="col-md-2">
 				<button type="submit" class="form-control btn btn-primary">Guardar</button>
 			</div>	
 		</div>
 	</form>
@endsection
