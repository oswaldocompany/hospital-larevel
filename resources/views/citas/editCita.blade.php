@extends('layouts.app')
@section('titulo','Editar Cita')

@section('content')
	<div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Editar cita</h3>
      </div>
    </div>
	<form method="POST" action="{{ route('cita.update',$clave) }}" enctype="multipart/form-data">
		{{ method_field('put') }}
		@csrf
		@foreach($editCita as $row)
 		<div class="row"> 
 			<div class="col-md-1"></div>
 			<div class="col-md-3">
 				<label for="patient_id">Paciente</label>
	 			<select class="form-control" name="patient_id" required>
	 				<option value="">-- Seleccione un paciente --</option>
	 				@foreach($pacientes as $key)
	 					<option value="{{ $key->id }}" {{ $row->patient_id == $key->id ? 'selected="selected"' : '' }}>{{$key->first_name .' '.$key->middle_name .' '. $key->last_name}}</option>
	 				@endforeach
	 			</select>
 			</div>	
 			<div class="col-md-3">
 				<label for="employee_id">Nombre del Doctor</label>
	 			<select class="form-control" name="employee_id" required>
	 				<option value="">-- Seleccione quién lo atenderá --</option>
	 				@foreach($doctores as $key)
	 					<option value="{{$key->id}}" {{ $row->employee_id == $key->id ? 'selected="selected"' : '' }}>{{$key->first_name.' '.$key->middle_name}}</option>
	 				@endforeach
	 			</select>
 			</div>	
 			<div class="col-md-3">
 				<label for="department_id">Área</label>
	 			<select class="form-control" name="department_id">
	 				<option value="">-- Seleccione a que departamento --</option>
	 				@foreach($areas as $key)
	 					<option value="{{$key->id}}" {{ $row->department_id == $key->id ? 'selected="selected"' : '' }}>{{$key->department}}</option>
	 				@endforeach
	 			</select>
 			</div>
 			<div class="col-md-1"></div>	
 		</div>

 		<div class="row">  	
 		<div class="col-md-1"></div>		
 			<div class="col-md-2">
 				<label for="date_appointment">Fecha de Cita</label>
	 			<input type="date" name="date_appointment" value="{{$row->date_appointment}}" class="form-control ">
 			</div>	
 			<div class="col-md-1">
 				<label for="time_start">Hora</label>
	 			<input type="datetime" name="time_start" value="{{$row->time_start}}" class="form-control ">
 			</div>	
	 		<div class="col-md-3">
 				<label for="patient_email">Correo del paciente</label>
	 			<input type="text" name="patient_email" value="{{$row->patient_email}}" class="form-control ">
 			</div>	
 			<div class="col-md-3">
 				<label for="patient_number">Número del paciente</label>
	 			<input type="phone" name="patient_number" value="{{$row->patient_number}}" class="form-control ">
 			</div>
 			<div class="col-md-1"></div>
 		</div>

 		<div class="row">
 			<div class="col-md-1"></div>
	 		<div class="col-md-9">
	 			<label for="message">Mensaje</label>
	 			<textarea name="message" class="form-control">{{$row->message}}</textarea>
	 		</div>	
 		</div>

 		<div class="row" style="margin-top: 1%">
 			<div class="col-md-1"></div>
	 		<div class="col-md-2">
 				<button type="submit" class="form-control btn btn-primary">Actualizar</button>
 			</div>	
 		</div>
 	@endforeach
 	</form>
@endsection
