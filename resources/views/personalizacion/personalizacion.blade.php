@extends('layouts.app')
@section('titulo','Personalización')

@section('content')
	<div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Personalización</h3>
      </div>
    </div>
	<form method="post" action="{{ route('personalizacion.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="row" style="padding-top: 10px;">
			<div class="col-md-5">
				<label for="path">Cambiar logo</label>
				<input type="file" name="path" class="form-control">
			</div>
			<div class="col-md-5">
				<label for="name">Nombre de la empresa</label>
				<input type="text" name="name" maxlength="14" class="form-control input_borderclass=">
			</div>

			{{-- <div class="row" style="margin-top: 1%"> --}}
		 		<div class="col-md-2" style="margin-top: 3%">
	 				<button type="submit" class="form-control btn btn-primary">Guardar</button>
	 			</div>	
	 		{{-- </div> --}}
		</div>
	</form>
	
@endsection
