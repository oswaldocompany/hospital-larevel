@extends('layouts.app')

@section('content')
<style>
.swal-wide{
    width:80% !important;
	margin-top: -20% !important;
	margin-left: -35% !important;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}
td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<div class="row">
    <div class="col-lg-8 offset-lg-1">
        <h4 class="page-title">Nueva Receta</h4>
    </div>
</div>
<form method="POST" action="{{ route('receta.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-3 offset-lg-1">
            <label for="consult_id">N° de previa consulta</label>
            <select class="form-control" name="consult_id" required>
                <option value="">Seleccione una consulta</option>
                @foreach($allConsultas as $row)
                <option value="{{$row->id}}">
                    {{$row->id." - ".$row->first_name." ".$row->last_name." - ".\Carbon\Carbon::parse($row->created_at)->format('d/m/Y')}}
                </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row offset-lg-1">
        <div class="col-md-2">
            <p>Peso</p>
            <input type="text" name="weight" placeholder="Peso (Kg)" class="form-control m-input" autocomplete="off">
        </div>
        <div class="col-md-2">
            <p>Talla</p>
            <input type="text" name="size" placeholder="Talla (Cm)" class="form-control m-input" autocomplete="off">
        </div>
        <div class="col-md-2">
            <p>Temp</p>
            <input type="text" name="temperature" placeholder="Temperatura" class="form-control m-input"
                autocomplete="off">
        </div>
        <div class="col-md-2">
            <p>Glucosa</p>
            <input type="text" name="glucose" placeholder="Glucosa" class="form-control m-input" autocomplete="off">
        </div>
        <div class="col-md-2">
            <p>Saturación</p>
            <input type="text" name="blood_pressure" placeholder="Saturación" class="form-control m-input"
                autocomplete="off">
        </div>
    </div>

    <hr />

    <div id="inputFormRow"></div>
    <div id="newRow"></div>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-2">
            <button id="addRow" type="button" class="btn btn-info">Agregar medicamento</button>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-8 offset-lg-1">
            <div class="form-group">
                <p>Diagnostico general</p>
                <textarea name="diagnostic" class="form-control" rows="3" cols="50" required></textarea>
            </div>
            <div class="form-group">
                <p>Instrucciones generales</p>
                <textarea name="general_instructions" class="form-control" rows="3" cols="50"></textarea>
            </div>
        </div>
    </div>
	
    <div class="row offset-lg-1">
        <div class="col-md-7">
            <button type="button" class="form-control btn btn-warning" onclick="return previewHistorial()">Historial
                Clinico</button>
        </div>
        <div class="col-md-2">
            <button type="submit" class="form-control btn btn-primary">Guardar</button>
        </div>
    </div>
</form>



<script type="text/javascript">
var flag = 1;
var consultas = '{!!$getConsultas!!}';
$("#addRow").click(function() {

    var html = '';
    html += '<div id="inputFormRow">';
    html += '<div class="row offset-lg-1">';
    html += '<div class="col-md-2">';
    html += '<p for="medicine">Medicamento</p>';
    html +=
        '<input type="text" name="medicine'+flag+'" placeholder="Medicamento" class="form-control m-input" autocomplete="off" required>';
    html += '</div>';
    html += '<div class="col-md-2">';
    html += '<p for="medicine">Dosis</p>';
    html +=
        '<input type="text" name="dosage'+flag+'" placeholder="Dosis" class="form-control m-input" autocomplete="off" required>';
    html += '</div>';
    html += '<div class="col-md-2">';
    html += '<p for="medicine">Cantidad</p>';
    html +=
        '<input type="text" name="pieces'+flag+'" placeholder="Piezas" class="form-control m-input" autocomplete="off" required>';
    html += '</div>';

    html += '<div class="col-md-2">';
    html += '<p for="medicine">Tiempo de aplicación</p>';
    html +=
        '<input type="text" name="days_application'+flag+'" placeholder="Tiempo de aplicación" class="form-control m-input" autocomplete="off" required>';
    html += '</div>';
    html += '</div>';

    html += '<div class="row offset-lg-1 col-lg-8">';
    html += '<div class="input-group md-3">';
    html +=
        '<input type="text" name="instructions'+flag+'" placeholder="Instrucciones por medicamento" class="form-control m-input" autocomplete="off" required>';
    html += '<div class="input-group-append">';
    html += '<button id="removeRow" type="button" class="btn btn-danger">Eliminar sección</button>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += ' <hr/>';
    html += '</div>';
    html += '<input type="hidden" id="count" name="count" value="'+flag+'">';

    flag = flag + 1;
    $('#newRow').append(html);
});

$(document).on('click', '#removeRow', function() {
    $(this).closest('#inputFormRow').remove();
});

function previewHistorial() {

    var htmlTable = '';
	htmlTable += '<table>';
	htmlTable += '<tr>';
	htmlTable += '<th>Fecha de Consulta</th>';
	htmlTable += '<th>Diagnostico</th>';
	htmlTable += '<th>Instrucciones Generales</th>';	
	htmlTable += '</tr>';
	
    for(row of consultas) {
        htmlTable += '<tr>';
    	htmlTable += '<td>'+row[created_at]+'</td>';
    	htmlTable += '<td>Faringitis agudisima conbinado con una seria muy seria alergia al trabajo</td>';
    	htmlTable += '<td></td>';
    	htmlTable += '</tr>';
    };

	htmlTable += '</table>';

    event.preventDefault();    
	swal({
       title: '',
       text: htmlTable,
       html: true,
       customClass: 'swal-wide',
       showCancelButton: true,
       showConfirmButton:false,
	   cancelButtonText: "Cerrar",
	   position: 'left'
   });
}
</script>

@endsection