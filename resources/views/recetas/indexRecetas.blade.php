@extends('layouts.app2')
@section('titulo','Ver Recetas')

@section('content')
  {{-- @if (Auth::user()->rol == 'Administrador') --}}
    
        @include("errors.alert-danger")
        @include("errors.alert-success")
        @include("errors.alert-warning")
        @include("errors.custom-alert")

        <head>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css"> 

        <script type="text/javascript">
          $(document).ready(function () {
            $(function () {
              $('#example2').DataTable({
                // "scrollY": "350px",
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language":{
                  "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
              });
           });
          });  
        </script>
      </head>

          {{-- <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                  <a href="{{url('usuario/create')}}" class="btn btn-success">
                    Agregar Usuario
                  </a>
              </div>
              
              <div class="col-md-4">
                <div class="input-group">
                  <span class="input-group-addon">Buscar</span>
                  <input id="filtrar" type="text" class="form-control" placeholder="Buscar">
                </div>
              </div>
            </div>
          </div> --}}
        {{-- <hr>
    </br>
    </br> --}}
    <br/>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-5">
        <h3>Todas las recetas</h3>
      </div>
    </div>
     <div class="row">
        <div class="col-md-4">
            <a href="{{url('receta/create')}}" class="btn btn-primary">
              Nuevo Registro
            </a>
        </div>
        <div class="col-md-4">
          {{-- <h3>Registros</h3> --}}
        </div>
      </div>
      </br>
        {{-- <div class="row"> --}}
          <div class="col-md-12" style="padding: initial;">
            <!-- {{-- <div class="table-wrapper-scroll-y my-custom-scrollbar"> --}} -->
              <table id="example2" class="table table-bordered table-hover"> <!--table-sm-->
              <thead>
                <tr>
                  <th style="text-align: center;">Id</th>
                  <th style="text-align: center;">Nombre</th>
                  <th style="text-align: center;">Fecha de creación</th>
                  {{-- <th style="text-align: center;">E-mail</th>
                  <th style="text-align: center;">Municipio</th> --}}
                  <th style="text-align: center;">Ver</th>
                  <th style="text-align: center;">Eliminar</th>
                 </tr>
              </thead>
                <tbody class="buscar">
                  @foreach($allRecetas as $valor)
                    {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
                    <tr class="table-light">
                      <td style="text-align: center;">{{$valor->codigo}}</td>
                      <td style="text-align: center;">{{$valor->first_name." ".$valor->middle_name." ".$valor->last_name}}</td>
                      <td style="text-align: center;">{{\Carbon\Carbon::parse($valor->created_at)->format('d/m/Y')}}</td>
                    {{--   <td style="text-align: center;">{{$valor->email}}</td>
                      <td style="text-align: center;">{{$valor->neighborhood}}</td> --}}

                      <td style="text-align: center;">
                          <?php $Id2 = $valor->codigo?>
                          <a class="btn btn-default" href="{{url('/receta/verReceta/'.encrypt($valor->codigo))}}"><img src="img/ver.png"/></a>
                      </td>


                      {{-- <td style="text-align: center;">
                          <a class="btn btn-default" href="{{url('/receta/'.encrypt($valor->codigo).'/edit')}}"><img src="img/ver.png"/></a>
                      </td> --}}

                      <td style="text-align: center;"> 
                         <form action="{{ route('receta.inactivar',$valor->codigo) }}" method="DELETE" id="form_deleted_col">
                             
                           <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->codigo}}">
                            <button class="btn btn-danger botonCus" type="submit" onclick="return AlertEliminar(event,{{ $valor->codigo }})">
                              <input type="hidden" name="id_eliminar" id="id_eliminar" value="{{$valor->codigo}}">
                              
                              <input class="btn btn-danger" type="image" src="img/eliminar.png"/>
                            </button> 
                        </form>     
                      </td>
                       {{--<td style="text-align: center;">
                          <a class="" onclick="return AlertEliminar(event,{{$valor->id}})"><img src="img/eliminar.png"/></a>
                      </td>--}}

                    </tr>
                  @endforeach

                </tbody>
              </table>
            {{-- </div> Div Scroll custom --}}
          </div>
         {{-- </div> --}}
         {{-- <div class="col-md-4">
                {{ $datos->links() }} Con esto maás la paignacion del controller vamos a poder crear una paginacion
          </div> --}}
        </div>
{{--    @else
    <div class="row">
      <div class="col-md-4"></div>
      <div class="alert alert-danger" id="alert-danger" style="display:block">
        <a class="close" onclick="$('.alert').fadeOut()">X</a>    
        <div id="text-alert-danger">
          ¡¡ No puedes accesar a esta ruta !!...
        </div>
      </div>
    </div>
  @endif --}}
@endsection

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


 <script type="text/javascript">

    function AlertEliminar(event, id) {
        event.preventDefault();
        // var route = BASE_URL + "cuarto/borrar/" + id;
        var route = BASE_URL + "receta/inactivar/" + id;
        // alert (route);
        swal({
          title: "¿Está seguro de eliminar este registro?",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, Eliminalo!",
          closeOnConfirm: false
      }
      , function (inputValue) {
          if (inputValue === false) return false;
        
          // var form = $("#form_deleted_col").attr("action", route);
          // $("#form_deleted_col").submit();
          window.location = route; 
            swal("Correcto!", "Se elimino correctamente", "success");
      }
      );   
    }
</script>