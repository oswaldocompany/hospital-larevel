@extends('layouts.app')
@section('titulo','Detalle de receta')

@section('content')
<br/>
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-5">
      <h3>Detalles de receta</h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    @foreach($query as $row)    
    <div class="col-md-5">
      <h5><strong>Nombre del paciente: </strong>{{$row->first_name." ".$row->middle_name." ".$row->last_name}}</h5>
    </div>
    <div class="col-md-5">
      <h5><strong>Fecha de creación: </strong>{{\Carbon\Carbon::parse($row->created_at)->format('d - m - Y')}}</h5>
    </div>
    @break
    @endforeach
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-1"></div>   

      <div class="col-md-12" style="padding: initial;">
        <table id="example2" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th style="text-align: center;">Articulo/Medicina</th>
            <th style="text-align: center;">Aplicación</th>
            <th style="text-align: center;">Cantidad</th>
            <th style="text-align: center;">Días de aplicación</th>
            <th style="text-align: center;">Instrucciones adicionales</th>
           </tr>
        </thead>
          <tbody class="buscar">
            @foreach($query as $valor)
              {{-- <?php $fechaTodo = $valor->fecha; ?> --}}
              <tr class="table-light">
                <td style="text-align: center;">{{$valor->medicine}}</td>
                <td style="text-align: center;">{{$valor->dosage}}</td>
                <td style="text-align: center;">{{$valor->pieces}}</td>
                <td style="text-align: center;">{{$valor->days_application}}</td>
                <td style="text-align: center;">{{$valor->instructions}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>


  </div>

@endsection
 {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> --}}
 {{-- <script src="{{url('js/jquery.dataTables.js')}}"></script> --}}
<script type="text/javascript">
  $(document).ready(function () {
    $(function () {
      $('#example2').DataTable({
        // "scrollY": "350px",
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "language":{
          "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
      });
   });
  });  
</script>