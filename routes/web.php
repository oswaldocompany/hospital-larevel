<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\doctoresController;
use App\Http\Controllers\{PacientesController,ConsultaController,AreasController,RecetaController,CitaController,RolController,PersonalizacionController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

// CONSULTA
Route::get('/showPatient',[ConsultaController::class, 'showPatient'])->name('consulta.showPatient');
Route::get('/consulta/createT/{id}',[ConsultaController::class, 'createT'])->name('consulta.createT');

// EMPLEADO
Route::get('/empleado/inactivar/{id}',[doctoresController::class, 'inactivar'])->name('empleado.inactivar');

// PACIENTE
Route::get('/paciente/inactivar/{id}',[PacientesController::class, 'inactivar'])->name('paciente.inactivar');
Route::get('paciente',[PacientesController::class, 'index'])->name('paciente.index');

// CONSULTA
Route::get('/consulta/inactivar/{id}',[ConsultaController::class, 'inactivar'])->name('consulta.inactivar');

// CITA
Route::get('/cita/inactivar/{id}',[CitaController::class, 'inactivar'])->name('cita.inactivar');

// RECETAS
Route::get('/receta/inactivar/{id}',[RecetaController::class, 'inactivar'])->name('receta.inactivar');
Route::get('/receta/verReceta/{id}',[RecetaController::class, 'verReceta'])->name('receta.verReceta');
Route::get('/returnIndex',[RecetaController::class, 'returnIndex'])->name('datatables.data');


// AREAS
Route::get('/area/inactivar/{id}',[AreasController::class, 'inactivar'])->name('area.inactivar');

//PERSONALIZACION
Route::get('/personalizacion/pantalla',[PersonalizacionController::class,'pantalla'])->name('personalizacion.pantalla');

Route::post('/personalizacion/store',[PersonalizacionController::class,'store'])->name('personalizacion.store');

Route::resources([
	'empleado' => doctoresController::class,
	'paciente' => PacientesController::class,
	'consulta' => ConsultaController::class,
	'area' => AreasController::class,
	'receta' => RecetaController::class,
	'cita' => CitaController::class,
	'rol' => RolController::class,
]);

Route::get('/soporte',[App\Http\Controllers\HomeController::class, 'soporte'])->name('soporte');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//limpiar caches
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
