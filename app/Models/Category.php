<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';

    protected $table = 'category';

    protected $guarded = [];

    protected $casts = [
    	'id' => 'integer',
    ];

    public function articles()
    {
    	return $this->hasMany(App\Models\Article::class);
    }
}
