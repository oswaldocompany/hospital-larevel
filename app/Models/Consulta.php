<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';
    
    protected $fillable = [
        'blood_pressure',
        'temperature',
        'weight',
        'stature',
        'size',
        'glucose',
        'saturation',
        'diagnostic',
        'general_instructions',
        'employee_id',
        'department_id',
        'patient_id',
        'company_id',
    ];

    protected $table = 'consult';
}
