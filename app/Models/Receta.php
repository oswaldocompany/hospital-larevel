<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';

    protected $table = 'prescription';

    protected $fillable = [
        'medicine',
        'dosage',
        'pieces',
        'days_application',
        'instructions',
        'consult_id',
    ];
}
