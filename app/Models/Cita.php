<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';

    protected $fillable = [
    	'patient_id',
    	'employee_id',
    	'department_id',
        'date_appointment',
        'time_start',
        'patient_email',
        'patient_number',
        'message',
        'company_id',
    ];

    protected $table = 'appointment';
}
