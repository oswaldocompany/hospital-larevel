<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';

    protected $casts = [
    	'id' => 'integer',
    	'category_id' => 'integer',
    	'user_id' => 'integer',
    ];

    public function fields()
    {
    	return [
        	'title'=>$this->title,
        	'slug'=>$this->slug,
        	'content'=>$this->content,     
    	];
    }

    public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}


