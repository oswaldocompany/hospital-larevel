<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'birth_date',
        'gender',
        'avatar',
        'street',
        'external_number',
        'internal_number',
        'neighborhood',
        'municipality',
        'city',
        'postal_code',
        'descripcion',
        'status',
        'company_id',
    ];

    protected $table = 'patient';
}
