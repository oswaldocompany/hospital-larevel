<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';
    

    protected $table = 'employee';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'email',
        'birth_date',
        'gender',
        'avatar',
        'biography',
        'street',
        'external_number',
        'internal_number',
        'neighborhood',
        'municipality',
        'city',
        'postal_code',
        'rol_id',
        'department_id',
        'status',
        'company_id'
    ];
}
