<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CANCELLED = 'INACTIVO';

    protected $fillable = [
        'department',
        'status',
        'company_id'
    ];

    protected $table = 'department';
}
