<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCita extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

         $data = [
            'patient_id' => ['required'],
            'employee_id' => ['required'],
            'department_id' => ['required'],
            'date_appointment' => ['required'],
            'time_start' => ['required'],
            'patient_email' => ['required'],
            'patient_number' => ['required'],
            'message' => ['required'],
        ];

        return $data;
    }

    public function message()
    {
        return[
            'date_appointment'=>'El campo fecha es requerido',
        ];
    }
}
