<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePaciente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            'first_name' => ['required','max:30'],
            'middle_name' => ['required','max:30'],
            'last_name' => ['max:30'],
            'phone' => ['required','max:10'],
            'email' => ['max:30'],
            'birth_date' => ['required'],
            'gender' => ['required'],
            'street' => ['required','max:50'],
            'external_number' => ['required','max:5'],
            'internal_number' => ['max:5'],
            'neighborhood' => ['required','max:50'],
            'municipality' => ['required','max:50'],
            'city' => ['required','max:50'],
            'postal_code' => ['required','max:5'],
            'descripcion' => ['max:255'],
            'company_id' => ['required'],
        ];

        return $data;
    }

    public function messages()
    {
        return [
            'first_name.max' => 'El campo nombre no debe contener más de 30 caracteres.',
            'middle_name.max' => 'El campo apellido paterno no debe contener más de 30 caracteres.',
            'last_name.max' => 'El campo apellido materno no debe contener más de 30 caracteres.',
            'phone.max' => 'El campo telefono no debe contener más de 10 caracteres.',
            'street.max' => 'El campo calle no debe contener más de 50 caracteres.',
            'external_number.max' => 'El campo numero exterior no debe contener más de 5 caracteres.',
            'internal_number.max' => 'El campo numero interior no debe contener más de 5 caracteres.',
            'neighborhood.max' => 'El campo colonia no debe contener más de 50 caracteres.',
            'municipality.max' => 'El campo municipio no debe contener más de 50 caracteres.',
            'city.max' => 'El campo ciudad no debe contener más de 50 caracteres.',
            'postal_code.max' => 'El campo código postal no debe contener más de 5 caracteres.',
            // 'descripcion.max' => 'El campo descripcion no debe contener más de 255 caracteres.',
        ];
    }
}
