<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreConsulta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blood_pressure' => ['required'],
            'temperature' => ['required'],
            'weight' => ['required'],
            'stature' => ['required'],
            'size' => ['required'],
            'glucose' => ['required'],
            'saturation' => ['required'],
            'diagnostic' => ['required'],
            'general_instructions' => ['required'],
            'employee_id' => ['required'],
            'department_id' => ['required'],
            'patient_id' => ['required'],
        ];
    }
}
