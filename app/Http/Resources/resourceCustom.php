<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class resourceCustom extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'articles',
             'id'=> (string) $article->getRouteKey(),
             'attibutes'=>$this->resource->fields(),
             'links'=> [
                 'self'=> route('api.v1.articles.show',$article)
             ]
        ];
    }
}
