<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePersonalizacion;
use Illuminate\Support\Facades\Storage;
use App\Models\Personalizacion;
use Illuminate\Support\Facades\Session;
use Auth;

class PersonalizacionController extends Controller
{
    public function pantalla()
    {
        return view('personalizacion.personalizacion');
    }

    public function store(StorePersonalizacion $request)
    {
        $file = $request->file('path');
        $nombre = $file->getClientOriginalName();
        $id = Auth::user()->company_id;
        // $search = Personalizacion::query()
        //         ->where('status','ACTIVE')
        //         ->where('id','=',$id)
        //         ->get();
       
        if($id){
            Storage::disk('public')->put($nombre,file_get_contents($file));
            $imagenUpdate = Personalizacion::find($id);
            $imagenUpdate->name = $request->name;
            $imagenUpdate->path = $nombre;
            $imagenUpdate->save();

            $tipo_mensaje = "success";
            $texto_mensaje = "Se han guardado tus datos";
        }
        
        if(!$request->ajax()) {
            // dd($request->id_d);
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect('/home');
        }
    }
}
