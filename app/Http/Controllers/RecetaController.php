<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Receta,Paciente,Consulta};
use DB;
use Auth;
use Carbon\Carbon;

class RecetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idUser="";
        $id = Auth::user()->company_id;

        $allRecetas = DB::select("SELECT DISTINCT A.codigo, B.id, C.first_name, C.middle_name, C.last_name, A.created_at FROM prescription as A, consult as B, patient as C WHERE a.consult_id = b.id and b.patient_id = c.id and a.status = 'ACTIVE'");        
        
        // dd($allRecetas);
        return view ('recetas.indexRecetas',compact('allRecetas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::user()->company_id;
        $allConsultas = DB::table('consult')
                    ->join('patient','consult.patient_id','=','patient.id')
                    // ->join('employee','consult.employee_id','=','employee.id')
                    ->select('consult.id','patient.first_name','patient.last_name','consult.created_at')
                    ->where('consult.status','=','ACTIVE')
                    ->where('consult.company_id',$id)
                    ->distinct()
                    ->get();
        // dd($allConsultas);

        $getConsultas = Consulta::query()
                    ->where('status','=','ACTIVE')
                    // ->where('patient_id',$idPaciente)
                    ->orderBy('created_at','desc')
                    ->take(3)
                    ->get();

        return view ('recetas.newRecetas',compact('allConsultas','getConsultas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = Auth::user()->company_id;
        // dd($id);
        $consulta = Consulta::create([
            'blood_pressure' => $request->blood_pressure,
            'temperature' => $request->temperature,
            'weight' => $request->weight,
            'stature' => $request->stature,
            'size' => $request->size,
            'glucose' => $request->glucose,
            'saturation' => null,
            'diagnostic' => $request->diagnostic,
            'general_instructions' => $request->general_instructions,
            'employee_id' => $request->employee_id,
            'department_id' => null,
            'patient_id' => $request->patient_id,
            'company_id' => $id,
        ]);

        $consulta->save();

        for ($i=1; $i <= $request->count; $i++) { 
            $medicine = "medicine".$i;
            $dosage = "dosage".$i;
            $pieces = "pieces".$i;
            $days_application = "days_application".$i;
            $instructions = "instructions".$i;

            // $id = Auth::user()->company_id;
            $receta = Receta::create([
                'medicine' => $request->$medicine,
                'dosage' => $request->$dosage,
                'pieces' => $request->$pieces,
                'days_application' => $request->$days_application,
                'instructions' => $request->$instructions,
                'consult_id' => $request->consult_id,
                'company_id' => $id,
                'codigo' => $this->generarCodigo($request->consult_id),
            ]);

        $receta->save();
        }
        
        
        return redirect()->to(route('receta.index'))->with('success','Creado correctamente');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inactivar(Receta $id)
    {
        // dd($id);
        $id->status = Receta::STATUS_CANCELLED;
        $id->save();

        return redirect()->to(url('/receta'))->with('success','Borrado');
    }

    public function generarCodigo($consulta)
    {
        $fecha = Carbon::now()->format('y-m-d');
        $codigo = $consulta."-".$fecha;

        return $codigo;
    }

    public function verReceta($id)
    {
        $codigo = decrypt($id);

        $query = DB::select("SELECT A.codigo, C.first_name, C.middle_name, C.last_name, A.created_at, A.medicine, A.dosage,A.pieces, A.days_application, A.instructions FROM prescription as A, consult as B, patient as C WHERE a.consult_id = b.id and b.patient_id = c.id and a.status = 'ACTIVE' and a.codigo = ".$codigo);

        // dd($query);

        return view('recetas.viewReceta',compact('query'));
    }
}
