<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\Paciente;
use App\Http\Requests\StorePaciente;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class PacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = Auth::user()->company_id;

        $allPatient = DB::table('patient')
                    ->distinct()
                    ->where('status','=','ACTIVE')
                    ->where('company_id',$id)
                    ->get();
        return view('pacientes.indexPatient',compact('allPatient'));
        // return view('pacientes.indexPatient');        
    }

    public function returnIndex()
    {
        $id = Auth::user()->company_id;
        // RETURN DE DATATABLES
        return Datatables::of(DB::table('patient')
                    ->distinct()
                    ->where('status','=','ACTIVE')
                    ->where('company_id',$id))->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::user()->company_id;
        return view('pacientes.newPatient',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $id = Auth::user()->company_id;

        try {

            if($request->file('avatar')){
                $file = $request->file('avatar');
                $nombre = $file->getClientOriginalName();  

                Storage::disk('public')->put($nombre,file_get_contents($file));                

            }else{
                $nombre = 'usuario.png';
            }                      

            $patient = Paciente::create([

                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'email' => $request->email,
                'birth_date' => $request->birth_date,
                'gender' =>   $request->gender,
                'avatar' => $nombre,
                'street' => $request->street,
                'external_number' => $request->external_number,           
                'internal_number' => $request->internal_number,
                'neighborhood' => $request->neighborhood,
                'municipality' => $request->municipality,
                'city' => $request->city,
                'postal_code' => $request->postal_code,         
                'descripcion' => $request->biography,
                'company_id' => $id
            ]);            

            $patient->save();
            return redirect()->to(route('paciente.index'))->with('success','Creado correctamente');

        } catch (Exception $e) {
            return redirect()->to(route('paciente.index'))->with('danger','Error');
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idE)
    {
        $clave = decrypt($idE);
        $id = Auth::user()->company_id;

        $editPatient = Paciente::query()
                    ->where('id','=',$clave)
                    ->where('company_id',$id)->latest()->get();

        return view('pacientes.editPatient',compact('editPatient','clave'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Paciente::where('id',$id)
            ->update([
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'email' => $request->email,
                'birth_date' => $request->birth_date,
                'gender' =>   $request->gender,
                // 'avatar' => $nombre,
                // 'biography' =>  $request->descripcion,
                'street' => $request->street,
                'external_number' => $request->external_number,           
                'internal_number' => $request->internal_number,
                'neighborhood' => $request->neighborhood,
                'municipality' => $request->municipality,
                'city' => $request->city,
                'postal_code' => $request->postal_code,         
                'descripcion' => $request->descripcion,
                // 'role_id' => $request->rol_id,
                // 'company_id' => $id,

            ]);

        return redirect()->to(route('paciente.index'))->with('success','Modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inactivar(Paciente $id)
    {
        // dd($id);
        $id->status = Paciente::STATUS_CANCELLED;
        $id->save();

        return redirect()->to(url('/paciente'))->with('success','Borrado');
    }
}
