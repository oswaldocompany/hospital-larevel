<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => Article::all()->map(function($article){
                return [
                    'type'=>'articles',
                    'id'=> (string) $article->getRouteKey(),
                    'attibutes'=>[
                        'title'=>$article->title,
                        'slug'=>$article->slug,
                        'content'=>$article->content,
                    ],

                    'links'=> [
                        'self'=> route('api.v1.articles.show', $article)
                    ]
                ];
            })
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return response()->json([
            'data' => [
                'type'=>'articles',
                'id'=> (string) $article->getRouteKey(),
                'attibutes'=>[
                    'title'=>$article->title,
                    'slug'=>$article->slug,
                    'content'=>$article->content,
                ],

                'links'=> [
                    'self'=> url('/api/v1/articles/'. $article->getRouteKey())
                ]
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
