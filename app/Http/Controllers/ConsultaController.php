<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Consulta,Paciente,Area};
use App\Http\Requests\StoreConsulta;
use DB;
use Auth;

class ConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->company_id;
        $allConsultas = DB::table('consult')
                    ->join('patient','consult.patient_id','=','patient.id')
                    ->join('department','consult.department_id','=','department.id')
                    ->join('employee','consult.employee_id','=','employee.id')
                    ->select('consult.id','patient.first_name','patient.last_name','department.department','consult.diagnostic','consult.general_instructions')
                    ->where('consult.status','=','ACTIVE')
                    ->where('consult.company_id',$id)
                    ->distinct()
                    ->get();

        // Consulta::query()->where('status','=','ACTIVE')->latest()->get();
        
        // dd($allConsultas);
        return view ('consultas.indexConsulta',compact('allConsultas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctores = Paciente::query()
                ->where('status','=','ACTIVE')
                ->get();

         $areas = Area::query()
                ->where('status','=','ACTIVE')
                ->latest()->get();

        return view ('consultas.newConsulta',compact('areas','doctores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->company_id;
        $consulta = Consulta::create([
            'blood_pressure' => $request->blood_pressure,
            'temperature' => $request->temperature,
            'weight' => $request->weight,
            'stature' => $request->stature,
            'size' => $request->size,
            'glucose' => $request->glucose,
            'saturation' => $request->saturation,
            'diagnostic' => $request->diagnostic,
            'general_instructions' => $request->general_instructions,
            'employee_id' => $request->employee_id,
            'department_id' => $request->department_id,
            'patient_id' => $request->patient_id,
            'company_id' => $id,
        ]);

        $consulta->save();
        return redirect()->to(route('consulta.index'))->with('success','Creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idE)
    {
        $clave = decrypt($idE);
        $id = Auth::user()->company_id;

        $editConsulta = Consulta::query()
                    ->where('id','=',$clave)
                    ->where('company_id',$id)->latest()->get();

        $nombrePaciente = Paciente::query()
                        ->select('first_name','middle_name','last_name','id')
                        ->where('id',$id)
                        ->get();

        $doctores = Paciente::query()
                ->where('status','=','ACTIVE')
                ->get();

        $areas = Area::query()
                ->where('status','=','ACTIVE')
                ->latest()->get();

        return view('consultas.editConsulta',compact('editConsulta','clave','nombrePaciente','doctores','areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Consulta::where('id',$id)
            ->update([
                'blood_pressure' => $request->blood_pressure,
                'temperature' => $request->temperature,
                'weight' => $request->weight,
                'stature' => $request->stature,
                'size' => $request->size,
                'glucose' => $request->glucose,
                'saturation' => $request->saturation,
                'diagnostic' => $request->diagnostic,
                'general_instructions' => $request->general_instructions,
                'employee_id' => $request->employee_id,
                'department_id' => $request->department_id,
                'patient_id' => $request->patient_id,
                // 'company_id' => $id,
            ]);

        return redirect()->to(route('consulta.index'))->with('success','Modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showPatient()
    {
        // dd("HOLA");
        $allEmployee = DB::table('patient')
                        ->distinct()
                        ->where('status','=','ACTIVE')
                        ->get();

        return view('consultas.viewPatient',compact('allEmployee'));
    }

    public function createT($id)
    {
        $idPaciente = "";
        $id = decrypt($id);

        $doctores = Paciente::query()
                ->where('status','=','ACTIVE')
                ->get();

        $areas = Area::query()
                ->where('status','=','ACTIVE')
                ->latest()->get();

        $nombrePaciente = Paciente::query()
                        ->select('first_name','middle_name','last_name','id')
                        ->where('id',$id)
                        ->get();
        foreach($nombrePaciente as $row){
            $idPaciente = $row->id;
        };

        $getConsultas = Consulta::query()
                    ->where('status','=','ACTIVE')
                    ->where('patient_id',$idPaciente)
                    ->orderBy('created_at','desc')
                    ->take(3)
                    ->get();
        // dd($idPaciente);

        return view ('consultas.newConsulta',compact('areas','doctores','id','nombrePaciente','getConsultas'));
    }

    public function inactivar(Consulta $id)
    {
        // dd($id);
        $id->status = Consulta::STATUS_CANCELLED;
        $id->save();

        return redirect()->to(url('/consulta'))->with('success','Borrado');
    }
}
