<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Http\Requests\StoreArea;
use Illuminate\View\View;
use Auth;

class AreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() : View
    {
        $id = Auth::user()->company_id;
        $allAreas = Area::query()
                    ->where('status','=','ACTIVE')
                    ->where('company_id',$id)->latest()->get();
        // dd($allAreas);
        return view('areas.indexArea',compact('allAreas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::user()->company_id;
        return view ('areas.newArea',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArea $request)
    {   //dd($request);
        $area = Area::create($request->validated());
        return redirect()->to(route('area.index'))->with('success','Creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idE)
    {
        $clave = decrypt($idE);
        $id = Auth::user()->company_id;

        $editArea = Area::query()
                    ->where('id','=',$clave)
                    ->where('company_id',$id)->latest()->get();

        return view('areas.editArea',compact('editArea','clave'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Area::where('id',$id)
            ->update([
                'department' => $request->department,
                'status' => $request->status
            ]);


        return redirect()->to(route('area.index'))->with('success','Modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inactivar(Area $id)
    {
        // dd($id);
        $id->status = Area::STATUS_CANCELLED;
        $id->save();

        return redirect()->to(url('/area'))->with('success','Borrado');
    }
}
