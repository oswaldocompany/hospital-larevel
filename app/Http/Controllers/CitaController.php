<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\{Doctor,Area,Paciente};
use App\Http\Requests\StoreCita;
use Illuminate\View\View;
use App\Models\Cita;

class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() : view
    {
        $id = Auth::user()->company_id;
        $allCitas = DB::table('appointment')
                    ->join('patient','appointment.patient_id','=','patient.id')
                    ->join('department','appointment.department_id','=','department.id')
                    ->join('employee','appointment.employee_id','=','employee.id')
                    ->select('appointment.id','patient.first_name','patient.last_name','department.department','appointment.date_appointment','appointment.patient_email')
                    ->where('appointment.status','=','ACTIVE')
                    ->where('appointment.company_id',$id)
                    ->distinct()
                    ->get();

        return view('citas.indexCita', compact('allCitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pacientes = Paciente::query()
                    ->where('status','=','ACTIVE')
                    ->get();
         // dd($pacientes);           

        $areas = Area::query()
                ->where('status','=','ACTIVE')
                ->latest()->get();

        $doctores = Doctor::query()
                    ->where('status','=','ACTIVE')
                    ->get();

        return view('citas.newCita',compact('pacientes','areas','doctores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->company_id;
        $cita = Cita::create([
            'patient_id' => $request->patient_id,
            'employee_id' => $request->employee_id,
            'department_id' => $request->department_id,
            'date_appointment' => $request->date_appointment,
            'time_start' => $request->time_start,
            'patient_email' => $request->patient_email,
            'patient_number' => $request->patient_number,
            'message' => $request->message,
            'company_id' => $id
        ]);

        // Enviar email **** PENDIENTE
        // Mail::send('mails.aprobacionSocio',$data, function($message)use($data) {
        //     $message->to($data['correo'])
        //     ->subject('Registro Aprobado');                
        //     // ->attachData($pdf->output(), "Cotizacion.pdf");
        // });

        $cita->save();
        return redirect()->to(route('cita.index'))->with('success','Creado correctamente');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idE)
    {
        $clave = decrypt($idE);
        $id = Auth::user()->company_id;

        $pacientes = Paciente::query()
                    ->where('status','=','ACTIVE')
                    ->get();

        $doctores = Doctor::query()
                    ->where('status','=','ACTIVE')
                    ->get();

        $areas = Area::query()
                ->where('status','=','ACTIVE')
                ->latest()->get();

        $editCita = Cita::query()
                    ->where('id','=',$clave)
                    ->where('company_id',$id)->latest()->get();
        // dd($editCita);
        return view('citas.editCita',compact('pacientes','doctores','areas','editCita','clave'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cita::where('id',$id)
            ->update([
                'patient_id' => $request->patient_id,
                'employee_id' => $request->employee_id,
                'department_id' => $request->department_id,
                'date_appointment' => $request->date_appointment,
                'time_start' => $request->time_start,
                'patient_email' => $request->patient_email,
                'patient_number' => $request->patient_number,
                'message' => $request->message,
                // 'company_id' => $id
            ]);

        return redirect()->to(route('cita.index'))->with('success','Modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inactivar(Cita $id)
    {
        // dd($id);
        $id->status = Cita::STATUS_CANCELLED;
        $id->save();

        return redirect()->to(url('/cita'))->with('success','Borrado');
    }
}
