<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\{ Doctor, Rol, Department };
use App\Http\Requests\StoreEmployee;
use Illuminate\Support\Facades\Storage;
use Auth;

class DoctoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->company_id;
        $allEmployee = DB::table('employee')
                        ->distinct()
                        ->where('status','=','ACTIVE')
                        ->where('company_id',$id)
                        ->get();
        //dd($allEmployee);    
        return view('empleados.indexEmpleado',compact('allEmployee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {           
        $roles = Rol::query()
                ->where('status','=','ACTIVE')
                ->where('rol','!=','Administrador')
                ->latest()->get();  
        $departamentos = Department::query()
                ->where('status', '=', 'ACTIVE')
                ->latest()->get();  
        return view('empleados.newEmpleado',compact('roles', 'departamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->company_id;
        //dd($id);
        try {

            if($request->file('avatar')){
                $file = $request->file('avatar');
                $nombre = $file->getClientOriginalName();  

                Storage::disk('public')->put($nombre,file_get_contents($file));                

            }else{
                $nombre = 'usuario.png';
            }
                      

            $employee = Doctor::create([
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'email' => $request->email,
                'birth_date' => $request->birth_date,
                'gender' =>   $request->gender,
                'avatar' => $nombre,
                'biography' =>  $request->biography,
                'street' => $request->street,
                'external_number' => $request->external_number,           
                'internal_number' => $request->internal_number,
                'neighborhood' => $request->neighborhood,
                'municipality' => $request->municipality,
                'city' => $request->city,
                'postal_code' => $request->postal_code,         
                'department_id' => $request->department_id,
                'role_id' => $request->rol_name,
                'company_id' => $id,
            ]);            
            dd($employee);
            $employee->save();
            return redirect()->to(route('empleado.index'))->with('success','Creado correctamente');

        } catch (Exception $e) {
            return redirect()->to(route('empleado.index'))->with('danger','Error');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idE)
    {
        $clave = decrypt($idE);
     
        $id = Auth::user()->company_id;
     
        $editEmploye = Doctor::query()
                    ->where('id','=',$clave)
                    ->where('company_id',$id)->latest()->get();                
     
        $gender = $editEmploye['0']->gender;
        $department_id = $editEmploye['0']->department_id;
        $role_id = $editEmploye['0']->role_id;
        
        $roles = Rol::query()
                ->where('status','=','ACTIVE')
                ->where('rol','!=','Administrador')
                ->latest()->get();

        $departamentos = Department::query()
                ->where('status', '=', 'ACTIVE')
                ->latest()->get();  

        return view('empleados.editEmpleado',compact('editEmploye','clave','roles', 'departamentos', 'gender', 'department_id', 'role_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        
        $idCompany = Auth::user()->company_id;
        
        if($request->file('avatar')){
            $file = $request->file('avatar');
            $nombre = $file->getClientOriginalName();  

            Storage::disk('public')->put($nombre,file_get_contents($file));                

        }else{
            $nombre = 'usuario.png';
        }

        Doctor::where('id',$id)
            ->update([
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'email' => $request->email,
                'birth_date' => $request->birth_date,
                'gender' =>   $request->gender,
                'avatar' => $nombre,
                'biography' =>  $request->biography,
                'street' => $request->street,
                'external_number' => $request->external_number,           
                'internal_number' => $request->internal_number,
                'neighborhood' => $request->neighborhood,
                'municipality' => $request->municipality,
                'city' => $request->city,
                'postal_code' => $request->postal_code,     
                'department_id' => $request->department_id,    
                'role_id' => $request->rol_id,
                'company_id' => $idCompany,

            ]);

        return redirect()->to(route('empleado.index'))->with('success','Modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
        // $id->status = Doctor::STATUS_CANCELLED;
        // $id->save();

        // return new FontsResource($id);
    }

    public function inactivar(Doctor $id)
    {
        // dd($id);
        $id->status = Doctor::STATUS_CANCELLED;
        $id->save();

        return redirect()->to(url('/empleado'))->with('success','Borrado');
    }
}
