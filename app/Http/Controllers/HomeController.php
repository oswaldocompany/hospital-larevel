<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Paciente,Cita,Doctor,Area};
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::user()->company_id;

        $paciente = Paciente::where('status', 'ACTIVE')->where('company_id',$id)->count();
        $cita = Cita::where('status', 'ACTIVE')->where('company_id',$id)->count();
        $doctor = Doctor::where('status', 'ACTIVE')->where('company_id',$id)->count();
        $area = Area::where('status', 'ACTIVE')->where('company_id',$id)->count();

        return view('home',compact('paciente','cita','doctor','area'));
    }

    public function soporte()
    {
        return view('soporte');
    }
}
